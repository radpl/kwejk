class AddAttachmentImageToMems < ActiveRecord::Migration
  def self.up
    change_table :mems do |t|
      t.attachment :image
    end
  end

  def self.down
    drop_attached_file :mems, :image
  end
end
